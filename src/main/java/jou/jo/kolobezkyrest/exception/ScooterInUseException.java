package jou.jo.kolobezkyrest.exception;

import jou.jo.kolobezkyrest.utils.Constants;
import org.springframework.http.HttpStatus;

/**
 * Exception to be thrown if scooter is in use and user try to rent it
 *
 * @author Richard Podolinsky
 */
public class ScooterInUseException extends KolobezkyException {


    private static final long serialVersionUID = 2287379781457571219L;

    public ScooterInUseException() {
        super();
    }

    public ScooterInUseException(final String message) {
        super(message);
    }

    @Override
    public HttpStatus getHttpStatusCode() {
        return HttpStatus.PRECONDITION_FAILED;
    }


    @Override
    public String getError() {
        return Constants.SCOOTER_IN_USE;
    }
}
