package jou.jo.kolobezkyrest.exception;

import jou.jo.kolobezkyrest.utils.Constants;
import org.springframework.http.HttpStatus;

/**
 * Exception to be thrown if resource is missing
 *
 * @author Richard Podolinsky
 */
public class NotFoundException extends KolobezkyException {

    private static final long serialVersionUID = 928581770313944748L;

    public NotFoundException() {
        super();
    }

    public NotFoundException(final String message) {
        super(message);
    }

    @Override
    public HttpStatus getHttpStatusCode() {
        return HttpStatus.NOT_FOUND;
    }


    @Override
    public String getError() {
        return Constants.NOT_FOUND;
    }
}
