package jou.jo.kolobezkyrest.exception;


import jou.jo.kolobezkyrest.utils.Constants;
import org.springframework.http.HttpStatus;

/**
 * Exception to be thrown if input preconditions are not met
 */
@SuppressWarnings("serial")
public class InvalidInputException extends KolobezkyException {

    public InvalidInputException() {
        super();
    }

    public InvalidInputException(final String message) {
        super(message);
    }

    @Override
    public HttpStatus getHttpStatusCode() {
        return HttpStatus.BAD_REQUEST;
    }


    @Override
    public String getError() {
        return Constants.INVALID_INPUT;
    }
}
