package jou.jo.kolobezkyrest.exception;

import org.springframework.http.HttpStatus;

public abstract class KolobezkyException extends RuntimeException {

    public KolobezkyException() {

    }

    public KolobezkyException(final String message) {
        super(message);
    }

    public abstract HttpStatus getHttpStatusCode();

    public abstract String getError();
}
