package jou.jo.kolobezkyrest.assembler;

import jou.jo.kolobezkyrest.data.domain.Location;
import jou.jo.kolobezkyrest.data.domain.Scooter;
import jou.jo.kolobezkyrest.data.pojos.LocationPojo;
import jou.jo.kolobezkyrest.data.pojos.ScooterPojo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author matos
 */
@Component
public class ScooterAssembler {

    @Autowired
    private ClientAssembler clientAssembler;

    public ScooterPojo assemblyScooterPojo(final Scooter scooter) {
        ScooterPojo scooterPojo = new ScooterPojo();
        scooterPojo.setName(scooter.getName());
        scooterPojo.setId(scooter.getId());
        scooterPojo.setCurrentClient(clientAssembler.assemblyClientPojo(scooter.getCurrentClient()));
        return scooterPojo;
    }

    public ScooterPojo assemblySimpleScooterPojo(final Scooter scooter) {
        ScooterPojo scooterPojo = new ScooterPojo();
        scooterPojo.setName(scooter.getName());
        scooterPojo.setId(scooter.getId());
        return scooterPojo;
    }

    public Scooter assemblyScooter(final ScooterPojo scooterPojo) {
        Scooter scooter = new Scooter();
        scooter.setName(scooterPojo.getName());
        return scooter;
    }


    public Scooter assemblyScooter(final Scooter scooter, final ScooterPojo scooterPojo) {

        scooter.setName(scooterPojo.getName());
        return scooter;
    }


    public LocationPojo assemblyLocationPojo(final Location location) {
        LocationPojo locationPojo = new LocationPojo();
        locationPojo.setUuid(location.getUuid());
        locationPojo.setLat(location.getLat());
        locationPojo.setLon(location.getLon());
        locationPojo.setSpeed(location.getSpeed());
        locationPojo.setTime(location.getTime());

        return locationPojo;
    }


    public Location assemblyLocation(final Long lat, final Long lon, final Long speed, final Scooter scooter) {
        Location location = new Location();

        location.setLat(lat);
        location.setLon(lon);
        location.setSpeed(speed);
        location.setTime(new Date());
        location.setScooter(scooter);

        return location;
    }

    public Page<ScooterPojo> assemblyScooterPojoPage(final Page<Scooter> entitiesPage) {
        return entitiesPage.map(scooterPojo -> assemblyScooterPojo(scooterPojo));
    }

    public List<LocationPojo> assemblyLocationList(final List<Location> locations) {
        return locations.stream().map(location -> assemblyLocationPojo(location)).collect(Collectors.toList());
    }

    public List<ScooterPojo> assemblyScooterPojoList(final List<Scooter> scooters) {
        return scooters.stream().map(scooter -> assemblyScooterPojo(scooter)).collect(Collectors.toList());
    }
}