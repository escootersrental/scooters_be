package jou.jo.kolobezkyrest.assembler;

import jou.jo.kolobezkyrest.data.domain.Client;
import jou.jo.kolobezkyrest.data.pojos.ClientPojo;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

/**
 * @author matos
 */
@Component
public class ClientAssembler {


    public ClientPojo assemblyClientPojo(final Client client) {
        if (client == null) {
            return null;
        }
        ClientPojo clientPojo = new ClientPojo();
        clientPojo.setName(client.getName());
        clientPojo.setSurname(client.getSurname());
        clientPojo.setUuid(client.getUuid());
        clientPojo.setEmail(client.getEmail());
        clientPojo.setPhone(client.getPhone());
        clientPojo.setIdCard(client.getIdCard());
        return clientPojo;
    }


    public Client assemblyClient(final ClientPojo clientPojo) {
        if (clientPojo == null) {
            return null;
        }
        Client client = new Client();
        client.setName(clientPojo.getName());
        client.setSurname(clientPojo.getSurname());

        client.setEmail(clientPojo.getEmail());
        client.setPhone(clientPojo.getPhone());
        client.setIdCard(clientPojo.getIdCard());
        return client;
    }


    public Client assemblyClient(Client client, final ClientPojo clientPojo) {

        client.setName(clientPojo.getName());
        client.setSurname(clientPojo.getSurname());

        client.setEmail(clientPojo.getEmail());
        client.setPhone(clientPojo.getPhone());
        client.setIdCard(clientPojo.getIdCard());
        return client;
    }

    public Page<ClientPojo> assemblyClientPojoPage(final Page<Client> entitiesPage) {
        return entitiesPage.map(clientPojo -> assemblyClientPojo(clientPojo));
    }

}