package jou.jo.kolobezkyrest.assembler;

import jou.jo.kolobezkyrest.data.domain.Rent;
import jou.jo.kolobezkyrest.data.pojos.RentPojo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

/**
 * @author matos
 */
@Component
public class RentAssembler {

    @Autowired
    private ScooterAssembler scooterAssembler;

    @Autowired
    private ClientAssembler clientAssembler;

    public RentPojo assemblyRentPojo(final Rent rent) {
        RentPojo rentPojo = new RentPojo();
        rentPojo.setId(rent.getId());
        rentPojo.setStart(rent.getStart());
        rentPojo.setEnd(rent.getEnd());
        rentPojo.setStatus(rent.getRentStatus());
        rentPojo.setScooter(scooterAssembler.assemblySimpleScooterPojo(rent.getScooter()));
        rentPojo.setClient(clientAssembler.assemblyClientPojo(rent.getClient()));

        return rentPojo;
    }


    public Page<RentPojo> assemblyRentPojoPage(final Page<Rent> entitiesPage) {
        return entitiesPage.map(rentPojo -> assemblyRentPojo(rentPojo));
    }

}