package jou.jo.kolobezkyrest.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import jou.jo.kolobezkyrest.data.pojos.LocationPojo;
import jou.jo.kolobezkyrest.data.pojos.RentPojo;
import jou.jo.kolobezkyrest.data.pojos.ScooterPojo;
import jou.jo.kolobezkyrest.service.ScooterService;
import jou.jo.kolobezkyrest.utils.ParamValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping(value = "/scooter")
@Api(value = "scooter", tags = {"scooter"}, description = "scooter API for Kolobezky")
public class ScooterController {

    @Autowired
    private ScooterService scooterService;

    @Autowired
    private ParamValidator paramValidator;

    @ApiOperation(value = "API for creating an scooter")
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<ScooterPojo> createScooter(@RequestBody final ScooterPojo scooterPojo) {

        return new ResponseEntity<>(scooterService.createScooter(scooterPojo), HttpStatus.CREATED);
    }


    @ApiOperation(value = "API for getting all scooters")
    @RequestMapping(value = "all", method = RequestMethod.POST)
    public ResponseEntity<List<ScooterPojo>> getAllScooter() {

        return new ResponseEntity<>(scooterService.getAllScooters(), HttpStatus.CREATED);
    }


    @ApiOperation(value = "API for getting a specific scooter based on id")
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<ScooterPojo> getScooter(
        @ApiParam(name = "id", value = "Scooter id")
        @PathVariable("id") final Long id) {

        return new ResponseEntity<>(scooterService.getScooterDetail(id), HttpStatus.OK);
    }


    @ApiOperation(value = "API for getting many scooter, optionally paginated")
    @RequestMapping(value = "", method = RequestMethod.GET)
    public ResponseEntity<Page<ScooterPojo>> getScooters(
        @ApiParam(name = "pgNum", value = "Page number of rooms")
        @RequestParam(value = "pgNum", required = false) final Integer pageNumber,
        @ApiParam(name = "pgSize", value = "Page size number of rooms")
        @RequestParam(value = "pgSize", required = false) final Integer pageSize) {

        return new ResponseEntity<>(scooterService.getScooters(paramValidator.validatePageParam(pageSize, pageNumber)), HttpStatus.OK);
    }


    @ApiOperation(value = "API for updating an existing scooter's information by scooter id")
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<ScooterPojo> updateScooter(
        @ApiParam(name = "id", value = "Updating scooter id")
        @PathVariable("id") final Long id,
        @RequestBody final ScooterPojo scooterPojo) {

        return new ResponseEntity<>(scooterService.updateScooter(id, scooterPojo), HttpStatus.OK);
    }


    @ApiOperation(value = "API for deleting an scooter by scooter id")
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<HttpStatus> deleteScooter(
        @ApiParam(name = "id", value = "Deleting scooter id")
        @PathVariable("id") final Long id) {

        scooterService.deleteScooter(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @ApiOperation(value = "API for adding location to a specific scooter")
    @RequestMapping(value = "/{id}/addLocation", method = RequestMethod.GET)
    public ResponseEntity<HttpStatus> addLocationScooter(
        @ApiParam(name = "scooterId", value = "scooter id")
        @PathVariable("scooterId") final Long scooterId,
        @ApiParam(name = "lat", value = "latitude")
        @PathVariable("lat") final Long lat,
        @ApiParam(name = "long", value = "longitude")
        @PathVariable("lon") final Long lon,
        @ApiParam(name = "speed", value = "speed")
        @PathVariable("speed") final Long speed) {

        scooterService.addLocationScooter(lat, lon, speed, scooterId);
        return new ResponseEntity<>(HttpStatus.OK);
    }


    @ApiOperation(value = "API for getting many scooter, optionally paginated")
    @RequestMapping(value = "{id}/location", method = RequestMethod.GET)
    public ResponseEntity<List<LocationPojo>> getScooters(
        @ApiParam(name = "id", value = "scooter id")
        @PathVariable("id") final Long id,
        @ApiParam(name = "limit", value = "limit of location records")
        @RequestParam(value = "limit", required = false) final Long limit) {

        return new ResponseEntity<>(scooterService.getScooterLocation(id, limit), HttpStatus.OK);
    }


    @ApiOperation(value = "API for creating an scooter")
    @RequestMapping(value = "{id}/clearLocations", method = RequestMethod.POST)
    public ResponseEntity<HttpStatus> createScooter(@ApiParam(name = "id", value = "scooter id")
                                                    @PathVariable("id") final Long id) {
        scooterService.clearLocations(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }


    @ApiOperation(value = "API for getting a borrow history by scooter")
    @RequestMapping(value = "/{id}/borrowHistory", method = RequestMethod.GET)
    public ResponseEntity<Page<RentPojo>> borrowHistory(
        @ApiParam(name = "id", value = "Scooter id")
        @PathVariable("id") final Long id,
        @ApiParam(name = "pgNum", value = "Page number of rooms")
        @RequestParam(value = "pgNum", required = false) final Integer pageNumber,
        @ApiParam(name = "pgSize", value = "Page size number of rooms")
        @RequestParam(value = "pgSize", required = false) final Integer pageSize) {

        return new ResponseEntity<>(scooterService.getScooterBorrowHistory(id, paramValidator.validatePageParam(pageSize, pageNumber)), HttpStatus.OK);
    }

}

