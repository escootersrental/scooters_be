package jou.jo.kolobezkyrest.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import jou.jo.kolobezkyrest.data.pojos.ClientPojo;
import jou.jo.kolobezkyrest.data.pojos.RentPojo;
import jou.jo.kolobezkyrest.data.pojos.ScooterPojo;
import jou.jo.kolobezkyrest.service.ClientService;
import jou.jo.kolobezkyrest.utils.ParamValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping(value = "/client")
@Api(value = "client", tags = {"client"}, description = "client API for Kolobezky")
public class ClientController {

    @Autowired
    private ClientService clientService;

    @Autowired
    private ParamValidator paramValidator;

    @ApiOperation(value = "API for creating client")
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<ClientPojo> createClient(@RequestBody final ClientPojo clientPojo) {

        return new ResponseEntity<>(clientService.createClient(clientPojo), HttpStatus.CREATED);
    }


    @ApiOperation(value = "API for getting a specific client based on uuid")
    @RequestMapping(value = "/{uuid}", method = RequestMethod.GET)
    public ResponseEntity<ClientPojo> getClient(
        @ApiParam(name = "uuid", value = "Client uuid")
        @PathVariable("uuid") final String uuid) {

        return new ResponseEntity<>(clientService.getClientDetail(uuid), HttpStatus.OK);
    }


    @ApiOperation(value = "API for updating an existing client's information by client uuid")
    @RequestMapping(value = "/{uuid}", method = RequestMethod.PUT)
    public ResponseEntity<ClientPojo> updateClient(
        @ApiParam(name = "uuid", value = "Updating client uuid")
        @PathVariable("uuid") final String uuid,
        @RequestBody final ClientPojo clientPojo) {

        return new ResponseEntity<>(clientService.updateClient(uuid, clientPojo), HttpStatus.OK);
    }


    @ApiOperation(value = "API for deleting an client by client uuid")
    @RequestMapping(value = "/{uuid}", method = RequestMethod.DELETE)
    public ResponseEntity<HttpStatus> deleteScooter(
        @ApiParam(name = "uuid", value = "Deleting client uuid")
        @PathVariable("uuid") final String uuid) {

        clientService.deleteClient(uuid);
        return new ResponseEntity<>(HttpStatus.OK);
    }


    @ApiOperation(value = "API for borrowing scooter")
    @RequestMapping(value = "{uuid}/borrow", method = RequestMethod.POST)
    public ResponseEntity<RentPojo> borrow(@ApiParam(name = "uuid", value = "Client uuid")
                                           @PathVariable("uuid") final String uuid,
                                           @RequestBody final ScooterPojo scooterPojo) {
        return new ResponseEntity<>(clientService.borrow(uuid, scooterPojo.getId()), HttpStatus.CREATED);
    }

    @ApiOperation(value = "API for give back scooter")
    @RequestMapping(value = "{uuid}/giveBack", method = RequestMethod.POST)
    public ResponseEntity<RentPojo> giveBack(@ApiParam(name = "uuid", value = "Client uuid")
                                             @PathVariable("uuid") final String uuid,
                                             @RequestBody final ScooterPojo scooterPojo) {
        return new ResponseEntity<>(clientService.giveBack(uuid, scooterPojo.getId()), HttpStatus.CREATED);
    }


    @ApiOperation(value = "API for getting a borrow history for client")
    @RequestMapping(value = "/{uuid}/borrowHistory", method = RequestMethod.GET)
    public ResponseEntity<Page<RentPojo>> borrowHistory(
        @ApiParam(name = "uuid", value = "Client uuid")
        @PathVariable("uuid") final String uuid,
        @ApiParam(name = "pgNum", value = "Page number of rooms")
        @RequestParam(value = "pgNum", required = false) final Integer pageNumber,
        @ApiParam(name = "pgSize", value = "Page size number of rooms")
        @RequestParam(value = "pgSize", required = false) final Integer pageSize) {

        return new ResponseEntity<>(clientService.getClientBorrowHistory(uuid, paramValidator.validatePageParam(pageSize, pageNumber)), HttpStatus.OK);
    }

}

