package jou.jo.kolobezkyrest.data.pojos;

import jou.jo.kolobezkyrest.data.enums.RentStatus;

import java.util.Date;

public class RentPojo {

    private Long id;
    private ScooterPojo scooter;
    private ClientPojo client;
    private Date start;
    private Date end;
    private RentStatus status;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ScooterPojo getScooter() {
        return scooter;
    }

    public void setScooter(ScooterPojo scooter) {
        this.scooter = scooter;
    }

    public ClientPojo getClient() {
        return client;
    }

    public void setClient(ClientPojo client) {
        this.client = client;
    }

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }

    public RentStatus getStatus() {
        return status;
    }

    public void setStatus(RentStatus status) {
        this.status = status;
    }
}
