package jou.jo.kolobezkyrest.data.pojos;

public class ScooterPojo {

    private String name;
    private Long id;
    private ClientPojo currentClient;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ClientPojo getCurrentClient() {
        return currentClient;
    }

    public void setCurrentClient(ClientPojo currentClient) {
        this.currentClient = currentClient;
    }
}
