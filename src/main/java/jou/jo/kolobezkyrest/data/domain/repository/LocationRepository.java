package jou.jo.kolobezkyrest.data.domain.repository;

import jou.jo.kolobezkyrest.data.domain.Location;
import jou.jo.kolobezkyrest.data.domain.Scooter;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface LocationRepository extends CrudRepository<Location, String> {

    @Query(value = "Select l from Location l where l.scooter_id = :scooterId Limit :limit", nativeQuery = true)
    List<Location> findAllBylimit(@Param("scooterId") Long scooterId, @Param("limit") Long limit);

    @Modifying
    @Query(value = "DELETE from Location l where l.scooter = :scooter")
    void deleteAllLocations(@Param("scooter") Scooter scooter);
}
