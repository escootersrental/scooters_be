package jou.jo.kolobezkyrest.data.domain.repository;

import jou.jo.kolobezkyrest.data.domain.Scooter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ScooterRepository extends CrudRepository<Scooter, Long> {

    @Query("Select s from Scooter s where s.id = :id and s.deleted = false")
    Scooter findScooterById(@Param("id") Long id);

    @Query("Select s from Scooter s where s.deleted = false")
    Page<Scooter> findScooterAll(Pageable pageable);


    @Query("Select s from Scooter s where s.deleted = false")
    List<Scooter> findScooterAll();


}
