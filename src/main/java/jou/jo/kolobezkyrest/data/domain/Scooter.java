package jou.jo.kolobezkyrest.data.domain;


import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "scooter")
public class Scooter implements Serializable {


    private static final long serialVersionUID = 3492967029947277430L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Basic
    private String name;

    @OneToMany(mappedBy = "scooter", fetch = FetchType.LAZY,
        cascade = CascadeType.ALL)
    private List<Rent> rents = new ArrayList<>();

    @ManyToOne
    @JoinColumn(name = "currentClient_uuid")
    private Client currentClient;

    @OneToMany(mappedBy = "scooter", fetch = FetchType.LAZY,
        cascade = CascadeType.ALL)
    private List<Location> locations = new ArrayList<>();

    @Column(name = "deleted", columnDefinition = "boolean default false")
    private boolean deleted = false;

    public List<Location> getLocations() {
        return locations;
    }

    public void setLocations(List<Location> locations) {
        this.locations = locations;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Rent> getRents() {
        return rents;
    }

    public void setRents(List<Rent> rents) {
        this.rents = rents;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public Client getCurrentClient() {
        return currentClient;
    }

    public void setCurrentClient(Client currentClient) {
        this.currentClient = currentClient;
    }
}
