package jou.jo.kolobezkyrest.data.domain.repository;

import jou.jo.kolobezkyrest.data.domain.Client;
import jou.jo.kolobezkyrest.data.domain.Rent;
import jou.jo.kolobezkyrest.data.domain.Scooter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface RentRepository extends CrudRepository<Rent, Long> {

    @Query("Select r from Rent r where r.scooter = :scooter and r.rentStatus = 'IN_RENT'")
    Rent findScooterInUse(@Param("scooter") Scooter scooter);


    @Query("Select r from Rent r where r.client = :client")
    Page<Rent> findRentHistoryByClient(@Param("client") Client client, Pageable pageable);

    @Query("Select r from Rent r where r.scooter = :scooter")
    Page<Rent> findRentHistoryByScooter(@Param("scooter") Scooter scooter, Pageable pageable);
}
