package jou.jo.kolobezkyrest.data.domain;


import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "location")
public class Location implements Serializable {

    private static final long serialVersionUID = -8892093805084105961L;

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "jou.jo.kolobezkyrest.utils.UUIDGenerator")
    private String uuid;

    @Basic
    private Long lat;

    @Basic
    private Long lon;

    @Temporal(TemporalType.TIMESTAMP)
    private Date time;

    @Basic
    private Long speed;

    @ManyToOne
    @JoinColumn(name = "scooter_id")
    private Scooter scooter;

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public Long getLat() {
        return lat;
    }

    public void setLat(Long lat) {
        this.lat = lat;
    }

    public Long getLon() {
        return lon;
    }

    public void setLon(Long lon) {
        this.lon = lon;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public Long getSpeed() {
        return speed;
    }

    public void setSpeed(Long speed) {
        this.speed = speed;
    }

    public Scooter getScooter() {
        return scooter;
    }

    public void setScooter(Scooter scooter) {
        this.scooter = scooter;
    }
}
