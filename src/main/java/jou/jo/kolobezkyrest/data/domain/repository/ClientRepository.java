package jou.jo.kolobezkyrest.data.domain.repository;

import jou.jo.kolobezkyrest.data.domain.Client;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface ClientRepository extends CrudRepository<Client, String> {

    @Query("Select c from Client c where c.uuid = :uuid and c.deleted = false")
    Client findByClientUuid(@Param("uuid") String uuid);

    @Query("Select c from Client c where c.deleted = false")
    Page<Client> findClientAll(Pageable pageable);
}
