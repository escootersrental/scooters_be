package jou.jo.kolobezkyrest.data.enums;

public enum RentStatus {
    FINISHED,
    IN_RENT
}
