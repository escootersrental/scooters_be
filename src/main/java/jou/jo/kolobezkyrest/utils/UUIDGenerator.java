package jou.jo.kolobezkyrest.utils;


import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.id.IdentifierGenerator;

import java.util.UUID;

public class UUIDGenerator implements IdentifierGenerator {

    @Override
    public String generate(SharedSessionContractImplementor sharedSessionContractImplementor, Object o) throws HibernateException {
        final UUID uuid = UUID.randomUUID();
        return uuid.toString();
    }
}

