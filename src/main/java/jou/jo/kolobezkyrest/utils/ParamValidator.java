package jou.jo.kolobezkyrest.utils;

import jou.jo.kolobezkyrest.exception.InvalidInputException;
import jou.jo.kolobezkyrest.exception.NotFoundException;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.sql.Timestamp;
import java.util.UUID;
import java.util.function.Supplier;

/**
 * Validator for parameters - null or empty values
 */
@Component
public class ParamValidator {

    /**
     * Validate if string param is not null and not empty, if is, exception is thrown
     *
     * @param paramName
     * @param paramValue
     */
    public void validateParam(final String paramName, final String paramValue) {
        if (StringUtils.isEmpty(paramValue)) {
            handleEmptyParam(paramName);
        }
    }

    /**
     * Validate if integer param is not null and not empty, if is, exception is thrown
     *
     * @param paramName
     * @param paramValue
     */
    public void validateParam(final String paramName, final Integer paramValue) {
        if (paramValue == null) {
            handleEmptyParam(paramName);
        }
    }

    /**
     * Validate if double param is not null and not empty, if is, exception is thrown
     *
     * @param paramName
     */
    public void validateParam(final String paramName, final Double paramValue) {
        if (paramValue == null) {
            handleEmptyParam(paramName);
        }
    }

    /**
     * check if uuid not null and is valid, if no, throw exception
     *
     * @param uuid
     */
    public void validateUuid(final UUID uuid) {
        handleUuidValidation("uuid", uuid);
    }

    /**
     * check if uuid not null and is valid, if no, throw exception
     *
     * @param uuidName - e.g. if we need to distinguish that this is kbase uuid
     * @param uuid
     */
    public void validateUuid(final String uuidName, final UUID uuid) {
        handleUuidValidation(uuidName, uuid);
    }

    /**
     * check if valida format of time and then create timestamp object
     *
     * @param timeInMillis string represents long time in millis
     * @return timestamp object initialized with data
     */
    public Timestamp validAndParseTimestamp(String paramName, String timeInMillis) {
        try {
            validateParam("", timeInMillis);
            long l = Long.parseLong(timeInMillis);

            return new Timestamp(l);
        } catch (Exception ex) {
            throw new InvalidInputException(paramName + " " + ex.getMessage());
        }
    }

    /**
     * Null check for objects get from repository
     *
     * @param clazz    class type
     * @param function which get the data
     * @return object got from function
     */
    @SuppressWarnings("unchecked")
    public <T> T withNullCheck(Class<? extends T> clazz, Supplier<T> function) {
        Object object = function.get();
        if (object == null) {
            throw new NotFoundException(String.format("%s with provided criteria does not exists.", new Object[]{clazz.getSimpleName()}));
        } else {
            return (T) object;
        }
    }

    /**
     * Null check for objects get from repository
     *
     * @param clazz        class type
     * @param function     which get the data
     * @param errorMessage custom message if data are null
     * @return object got from function
     */
    @SuppressWarnings("unchecked")
    public <T> T withNullCheck(Class<? extends T> clazz, Supplier<T> function, String errorMessage) {
        Object object = function.get();
        if (object == null) {
            throw new NotFoundException(errorMessage);
        } else {
            return (T) object;
        }
    }

    private void handleUuidValidation(final String uuidName, final UUID uuid) {
        // check if not null, then if valid
        if (uuid == null) handleEmptyParam(uuidName);
        try {
            UUID.fromString(uuid.toString());
        } catch (IllegalArgumentException exception) {
            throw new InvalidInputException(uuidName + " is not valid");
        }
    }

    private void handleEmptyParam(final String paramValue) {
        throw new InvalidInputException("" + paramValue + " parameter is empty or missing");
    }


    public Pageable validatePageParam(Integer pageSize, Integer pageNumber) {
        int pgNum = pageNumber == null ? 0 : pageNumber.intValue();
        int pgSz;
        if (pageSize != null && pageSize.intValue() > 0) {
            if (pageSize.intValue() > Constants.MAX_PAGE_SIZE) {
                pgSz = Constants.MAX_PAGE_SIZE;
            } else {
                pgSz = pageSize.intValue();
            }
        } else {
            pgSz = Constants.DEFAULT_PAGE_SIZE;
        }

        return new PageRequest(pgNum, pgSz);
    }

}

