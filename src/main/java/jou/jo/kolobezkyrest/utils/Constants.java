package jou.jo.kolobezkyrest.utils;

public class Constants {
    public static final String ERROR_MSG_PREFIX = "ERROR_";

    public static final Long DEFAULT_LIMIT = 100L;

    public static final int DEFAULT_PAGE_SIZE = 30;
    public static final int MAX_PAGE_SIZE = 100;


    public static final String INVALID_INPUT = ERROR_MSG_PREFIX + "INVALID_INPUT";
    public static final String NOT_FOUND = ERROR_MSG_PREFIX + "NOT_FOUND";
    public static final String SCOOTER_IN_USE = ERROR_MSG_PREFIX + "SCOOTER_IN_USE";

}
