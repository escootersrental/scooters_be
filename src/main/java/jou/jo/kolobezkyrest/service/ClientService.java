package jou.jo.kolobezkyrest.service;


import jou.jo.kolobezkyrest.assembler.ClientAssembler;
import jou.jo.kolobezkyrest.assembler.RentAssembler;
import jou.jo.kolobezkyrest.data.domain.Client;
import jou.jo.kolobezkyrest.data.domain.Rent;
import jou.jo.kolobezkyrest.data.domain.Scooter;
import jou.jo.kolobezkyrest.data.domain.repository.ClientRepository;
import jou.jo.kolobezkyrest.data.domain.repository.RentRepository;
import jou.jo.kolobezkyrest.data.domain.repository.ScooterRepository;
import jou.jo.kolobezkyrest.data.enums.RentStatus;
import jou.jo.kolobezkyrest.data.pojos.ClientPojo;
import jou.jo.kolobezkyrest.data.pojos.RentPojo;
import jou.jo.kolobezkyrest.exception.ScooterInUseException;
import jou.jo.kolobezkyrest.utils.ParamValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Date;

@Service
@Transactional
public class ClientService {

    @Autowired
    private ParamValidator paramValidator;

    @Autowired
    private ClientAssembler clientAssembler;

    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private ScooterRepository scooterRepository;

    @Autowired
    private RentRepository rentRepository;

    @Autowired
    private RentAssembler rentAssembler;


    public ClientPojo createClient(ClientPojo ClientPojo) {
        ClientPojo.setUuid(null);
        Client client = clientAssembler.assemblyClient(ClientPojo);
        clientRepository.save(client);
        return clientAssembler.assemblyClientPojo(client);
    }


    public ClientPojo updateClient(String uuid, ClientPojo clientPojo) {
        Client client = paramValidator.withNullCheck(Client.class, () -> clientRepository.findByClientUuid(uuid));
        client = clientAssembler.assemblyClient(client, clientPojo);
        return clientAssembler.assemblyClientPojo(client);
    }


    public ClientPojo getClientDetail(String uuid) {
        Client client = paramValidator.withNullCheck(Client.class, () -> clientRepository.findByClientUuid(uuid));
        return clientAssembler.assemblyClientPojo(client);
    }

    public void deleteClient(String uuid) {
        Client client = paramValidator.withNullCheck(Client.class, () -> clientRepository.findByClientUuid(uuid));
        client.setDeleted(true);
        clientRepository.save(client);
    }

    public Page<ClientPojo> getClients(Pageable pageable) {
        Page<Client> clients = clientRepository.findClientAll(pageable);
        return clientAssembler.assemblyClientPojoPage(clients);
    }

    public RentPojo borrow(String uuid, Long scooterId) {
        Client client = paramValidator.withNullCheck(Client.class, () -> clientRepository.findByClientUuid(uuid));
        Scooter scooter = paramValidator.withNullCheck(Scooter.class, () -> scooterRepository.findScooterById(scooterId));

        Rent rentScooterInUse = rentRepository.findScooterInUse(scooter);
        if (rentScooterInUse != null) {
            throw new ScooterInUseException();
        }
        Rent rent = new Rent();
        rent.setScooter(scooter);
        rent.setClient(client);
        rent.setRentStatus(RentStatus.IN_RENT);
        rent.setStart(new Date());

        rentRepository.save(rent);
        return rentAssembler.assemblyRentPojo(rent);
    }

    public RentPojo giveBack(String uuid, Long scooterId) {
        Client client = paramValidator.withNullCheck(Client.class, () -> clientRepository.findByClientUuid(uuid));
        Scooter scooter = paramValidator.withNullCheck(Scooter.class, () -> scooterRepository.findScooterById(scooterId));

        Rent rentScooterInUse = rentRepository.findScooterInUse(scooter);

        rentScooterInUse.setEnd(new Date());
        rentScooterInUse.setRentStatus(RentStatus.FINISHED);

        rentRepository.save(rentScooterInUse);
        return rentAssembler.assemblyRentPojo(rentScooterInUse);
    }

    public Page<RentPojo> getClientBorrowHistory(String clientUuid, Pageable pageable) {
        Client client = paramValidator.withNullCheck(Client.class, () -> clientRepository.findByClientUuid(clientUuid));
        Page<Rent> rents = rentRepository.findRentHistoryByClient(client, pageable);

        return rentAssembler.assemblyRentPojoPage(rents);
    }
}
