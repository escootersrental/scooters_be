package jou.jo.kolobezkyrest.service;


import jou.jo.kolobezkyrest.assembler.RentAssembler;
import jou.jo.kolobezkyrest.assembler.ScooterAssembler;
import jou.jo.kolobezkyrest.data.domain.Location;
import jou.jo.kolobezkyrest.data.domain.Rent;
import jou.jo.kolobezkyrest.data.domain.Scooter;
import jou.jo.kolobezkyrest.data.domain.repository.LocationRepository;
import jou.jo.kolobezkyrest.data.domain.repository.RentRepository;
import jou.jo.kolobezkyrest.data.domain.repository.ScooterRepository;
import jou.jo.kolobezkyrest.data.pojos.LocationPojo;
import jou.jo.kolobezkyrest.data.pojos.RentPojo;
import jou.jo.kolobezkyrest.data.pojos.ScooterPojo;
import jou.jo.kolobezkyrest.utils.Constants;
import jou.jo.kolobezkyrest.utils.ParamValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class ScooterService {

    @Autowired
    private ParamValidator paramValidator;

    @Autowired
    private ScooterAssembler scooterAssembler;

    @Autowired
    private ScooterRepository scooterRepository;

    @Autowired
    private LocationRepository locationRepository;

    @Autowired
    private RentRepository rentRepository;

    @Autowired
    private RentAssembler rentAssembler;

    public ScooterPojo createScooter(ScooterPojo scooterPojo) {
        scooterPojo.setId(null);
        Scooter scooter = scooterAssembler.assemblyScooter(scooterPojo);
        scooterRepository.save(scooter);
        return scooterAssembler.assemblyScooterPojo(scooter);
    }


    public ScooterPojo updateScooter(Long id, ScooterPojo scooterPojo) {
        Scooter scooter = paramValidator.withNullCheck(Scooter.class, () -> scooterRepository.findScooterById(id));
        scooter = scooterAssembler.assemblyScooter(scooter, scooterPojo);
        return scooterAssembler.assemblyScooterPojo(scooter);
    }


    public ScooterPojo getScooterDetail(Long id) {
        Scooter scooter = paramValidator.withNullCheck(Scooter.class, () -> scooterRepository.findScooterById(id));
        return scooterAssembler.assemblyScooterPojo(scooter);
    }

    public void deleteScooter(Long id) {
        Scooter scooter = paramValidator.withNullCheck(Scooter.class, () -> scooterRepository.findScooterById(id));
        scooter.setDeleted(true);
        scooterRepository.save(scooter);
    }

    public Page<ScooterPojo> getScooters(Pageable pageable) {
        Page<Scooter> scooters = scooterRepository.findScooterAll(pageable);
        return scooterAssembler.assemblyScooterPojoPage(scooters);
    }

    public List<LocationPojo> getScooterLocation(Long id, Long limit) {
        paramValidator.withNullCheck(Scooter.class, () -> scooterRepository.findScooterById(id));
        if (limit == null) {
            limit = Constants.DEFAULT_LIMIT;
        }
        List<Location> locations = locationRepository.findAllBylimit(id, limit);
        return scooterAssembler.assemblyLocationList(locations);
    }

    public void clearLocations(Long id) {
        Scooter scooter = paramValidator.withNullCheck(Scooter.class, () -> scooterRepository.findScooterById(id));
        locationRepository.deleteAllLocations(scooter);
    }

    public void addLocationScooter(Long lat, Long lon, Long speed, Long scooterId) {
        Scooter scooter = paramValidator.withNullCheck(Scooter.class, () -> scooterRepository.findScooterById(scooterId));
        Location location = scooterAssembler.assemblyLocation(lat, lon, speed, scooter);
        locationRepository.save(location);
    }

    public List<ScooterPojo> getAllScooters() {
        List<Scooter> scooters = scooterRepository.findScooterAll();
        return scooterAssembler.assemblyScooterPojoList(scooters);
    }

    public Page<RentPojo> getScooterBorrowHistory(Long scooterId, Pageable pageable) {
        Scooter scooter = paramValidator.withNullCheck(Scooter.class, () -> scooterRepository.findScooterById(scooterId));
        Page<Rent> rents = rentRepository.findRentHistoryByScooter(scooter, pageable);

        return rentAssembler.assemblyRentPojoPage(rents);
    }

}
