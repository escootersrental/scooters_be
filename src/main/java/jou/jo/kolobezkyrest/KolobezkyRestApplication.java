package jou.jo.kolobezkyrest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
@ComponentScan({"jou.jo.kolobezkyrest"})
@EntityScan({"jou.jo.kolobezkyrest"})
@EnableJpaRepositories({"jou.jo.kolobezkyrest"})
public class KolobezkyRestApplication {

    public static void main(final String[] args) {
        SpringApplication.run(KolobezkyRestApplication.class, args);
    }

}
